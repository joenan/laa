{
    "name": "Sales Management for Real Estate/Developer",
    "version": "10.1",
    "depends": ['sale'],
    "author": "Joenan",
    "category": "",
    "description" : """Sales Management for Real Estate/Developer""",
    'data': [
        'data/commission_data.xml',
        'views/sale_view.xml',
        'views/sale_config_settings_views.xml',
        'wizard/bf_payment_wizard_view.xml'
    ],
    'demo':[     
    ],
    'test':[
    ],
    'application' : True,
    'installable' : True,
    'auto_install' : False,
}
