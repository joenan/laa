from datetime import date
from datetime import datetime
# from datetime import timedelta
# from dateutil import relativedelta
# import time

from odoo import models, fields, api, _
from openerp.exceptions import UserError
# from openerp.tools.safe_eval import safe_eval as eval
# from openerp.tools.translate import _

class BfPaymentWizard(models.TransientModel):
    _name = 'bf.payment.wizard'
    
    date = fields.Date(string='Payment Date', required=True)
    amount = fields.Float('Amount', required=True)
    ref = fields.Char('Payment Reference')

    def action_confirm(self):
        if self.amount <= 0.0:
            raise UserError(_('Please input amount.'))
        self.env['sale.order'].browse(self._context.get('active_id')).action_create_bf(self.date, self.amount, self.ref)
        return True
