from datetime import date
from datetime import datetime
# from datetime import timedelta
# from dateutil import relativedelta
# import time

from odoo import models, fields, api, _
from openerp.exceptions import UserError
# from openerp.tools.safe_eval import safe_eval as eval
# from openerp.tools.translate import _


class SaleOrder(models.Model):
    _inherit = 'sale.order'
    
    bf_type = fields.Selection([('deposit', 'Deposit'),
                                ('income', 'Income')], string='Booking Fee Type', required=True,
                               default=lambda self: self.env.user.company_id.default_bf_type,
                               readonly=True, states={'draft': [('readonly', False)]})
    bf_move_id = fields.Many2one('account.move', string='Booking Fee', readonly=True)
    commission_inv_id = fields.Many2one('account.invoice', string='Commission', readonly=True)
    state = fields.Selection(selection_add=[('draft', 'Draft'),
                                            ('booked', 'Booked'),
                                            ('installment', 'Installment'),
                                            ('closed', 'Closed'),])
    
    @api.multi
    def action_bf_payment(self):
        return {
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'bf.payment.wizard',
                'type': 'ir.actions.act_window',
                'target': 'new',
                'context': self._context,
            }
    
    def action_create_bf(self, date, amount, ref):
        am_obj = self.env['account.move']
        aml_obj = self.env['account.move.line']
        
        # get journal
        journal_id = self.company_id.bf_journal_id_setting.id
        if not journal_id:
            raise UserError(_('Please set booking fee journal in Sale Settings.'))
        
        # get account
        account_id = self.partner_id.property_account_receivable_id.id if self.bf_type == 'deposit' \
            else self.company_id.set_bf_account_id_defaults.id
        if not account_id:
            raise UserError(_('Please set account in Sale Settings as well as in Partner Configuration.'))
        
        
        # kas/bank line
        aml1_vals = {
            'name': 'Booking Fee ' + self.order_line[0].name + ' - ' + self.name,
            'account_id': self.env['account.journal'].browse(journal_id).default_debit_account_id.id,
            'debit': amount,
            'credit': 0}
        # income/expense line
        aml2_vals = {
            'name': 'Booking Fee ' + self.order_line[0].name + ' - ' + self.name,
            'account_id': account_id,
            'partner_id': self.partner_id.id if self.bf_type == 'deposit' else False,
            'debit': 0,
            'credit': amount}
        line = [(0, 0, aml2_vals), (0, 0, aml1_vals)]

        move_vals = {
            'journal_id': self.env['account.journal'].browse(journal_id).id,
            'date': date,
            'company_id': self.env.user.company_id.id,
            'ref': ref,
            'line_ids': line}
        move_id = am_obj.create(move_vals)

        self.bf_move_id = move_id.id
        self.state = 'booked'
        
    @api.multi
    def action_create_invoice(self):
        i = max([line.installment for line in self.order_line])
        while i > 0:
            lines = []
            for line in self.order_line:
                if i <= line.installment:
                    account_id = line.product_id.property_account_income_id.id or \
                        line.product_id.categ_id.property_account_income_categ_id.id
                    line_vals = {
                        'product_id': line.product_id.id,
                        'name': line.name + ' - Installment ' + str(i),
                        'account_id': account_id,
                        'quantity': line.product_uom_qty,
                        'price_unit': line.price_unit / line.installment,
                        'uom_id': line.product_uom.id,
                        'invoice_line_tax_ids': [tax.id for tax in line.tax_id],
                        'sale_line_ids': [(6, 0, [line.id])]}
                    lines += [(0, 0, line_vals)]
            
            inv_vals = {
                'partner_id': self.partner_id.id,
                'account_id': self.partner_id.property_account_receivable_id.id,
                'payment_term_id': self.payment_term_id.id,
                'journal_id': self.env['account.invoice'].default_get(['journal_id'])['journal_id'],
                'company_id': self.env.user.company_id.id,
                'origin': self.name,
                'invoice_line_ids': lines}
            inv_id = self.env['account.invoice'].create(inv_vals)
            i -= 1
        self.state = 'installment'
        
    @api.multi
    def _generate_inv_commission(self):
        percent = self.company_id.commission_percent
        amount = self.amount_total * percent / 100
        product = self.company_id.commission_product_id
        if not product:
            raise UserError(_('Please set product in Sale Settings.'))
        line_vals = {
            'product_id': product.id,
            'name': 'Commission of ' + self.name,
            'account_id': product.property_account_expense_id.id,
            'quantity': 1,
            'price_unit': self.amount_total * self.company_id.commission_percent / 100,
            'uom_id': product.uom_id.id,
#             'invoice_line_tax_ids': [(6, 0, [tax.id for tax in product.supplier_taxes_id])]
        }
        lines = [(0, 0, line_vals)]
        inv_vals = {
            'type': 'in_invoice',
            'commission': True,
            'partner_id': self.user_id.partner_id.id,
            'account_id': self.user_id.partner_id.property_account_payable_id.id,
            'journal_id': self.env['account.invoice'].default_get(['journal_id'])['journal_id'],
            'company_id': self.env.user.company_id.id,
#             'date_invoice': self.name,
            'invoice_line_ids': lines}
        inv = self.env['account.invoice'].create(inv_vals)
        self._cr.execute("UPDATE sale_order SET commission_inv_id=%s WHERE id=%s", (inv.id, self.id))
        
        
class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'
    
    installment = fields.Integer(string='Installment', default=1, required=True,
                                 readonly=True, states={'draft': [('readonly', False)]})
    installment_done = fields.Boolean(string='Installment Done', compute='_set_done', store=True)
    invoice_paid = fields.Float(string='Invoice Paid', compute='_get_invoice_paid', store=True)

    @api.depends('invoice_lines.invoice_id.state')
    def _set_done(self):
        for order_line in self:
            if all(inv_line.invoice_id.state == 'paid' for inv_line in order_line.invoice_lines):
                order_line.installment_done = True
                # set sale order done
                if all(line.installment_done for line in order_line.order_id.order_line):
                    self._cr.execute("UPDATE sale_order SET state='closed' WHERE id=%s", (order_line.order_id.id,))
            else:
                order_line.installment_done = True
                self._cr.execute("UPDATE sale_order SET state='installment' WHERE id=%s", (order_line.order_id.id,))
            
    @api.depends('invoice_lines.invoice_id.state')
    def _get_invoice_paid(self):
        for order_line in self:
            # total of invoice paid on 1 sale order line
            # taken from unit price on invoice line
            amount = sum([inv_line.price_unit if inv_line.invoice_id.state == 'paid' \
                                           else 0 for inv_line in order_line.invoice_lines])
            order_line.invoice_paid = amount
            
            # create commission
            all_lines_amount = sum([line.invoice_paid for line in order_line.order_id.order_line])
            if not order_line.order_id.commission_inv_id and \
            all_lines_amount / order_line.order_id.amount_total >= order_line.order_id.company_id.commission_treshhold / 100:
                order_line.order_id._generate_inv_commission()
            
class AccountInvoice(models.Model):
    _inherit = 'account.invoice'
    
    commission = fields.Boolean(string='Commission')