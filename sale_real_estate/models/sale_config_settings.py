# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import logging

from odoo import api, fields, models

_logger = logging.getLogger(__name__)


class SaleConfiguration(models.TransientModel):
    _inherit = 'sale.config.settings'

    default_bf_type = fields.Selection([('deposit', 'Deposit'),
                                        ('income', 'Income')], string='Default Booking Fee Type', default='deposit',
                                       related='company_id.default_bf_type')
    bf_account_id_setting = fields.Many2one('account.account', 'Booking Fee Income Account',
        help='Default income account used for booking fee', related='company_id.bf_account_id_setting')
    bf_journal_id_setting = fields.Many2one('account.journal', 'Booking Fee Payment Method',
        help='Default journal used for booking fee payment', related='company_id.bf_journal_id_setting')
    default_invoice_policy = fields.Selection(selection_add=[('installment', 'Invoice based on installment')])
    commission_percent = fields.Float(string="Commission (%)", related='company_id.commission_percent')
    commission_product_id = fields.Many2one('product.product', string='Commission Product',
                                                    related='company_id.commission_product_id')
    commission_treshhold = fields.Float(string="Commission Treshhold (% of total value)", related='company_id.commission_treshhold')
    
class ResCompany(models.Model):
    _inherit = 'res.company'
    
    default_bf_type = fields.Selection([('deposit', 'Deposit'),
                                        ('income', 'Income')], string='Default Booking Fee Type')
    bf_account_id_setting = fields.Many2one('account.account', 'Booking Fee Income Account',
        help='Default income account used for booking fee')
    bf_journal_id_setting = fields.Many2one('account.journal', 'Booking Fee Payment Method',
        help='Default journal used for booking fee payment')
    commission_percent = fields.Float(string="Commission (%)")
    commission_product_id = fields.Many2one('product.product', string='Commission Expense Account')
    commission_treshhold = fields.Float(string="Commission Treshhold (% of total value)")
    
    
